# constantes

Reusable constants for different projects.

- [Source code](https://gitlab.com/joaommpalmeiro/constantes)
- [npm package](https://www.npmjs.com/package/constantes)
- [API](https://paka.dev/npm/constantes@0.2.0/api)
- [Licenses](https://licenses.dev/npm/constantes/0.2.0)
- [Package Phobia](https://packagephobia.com/result?p=constantes@0.2.0)
- [bundlejs](https://bundlejs.com/?bundle&q=constantes%400.2.0)
- [npm trends](https://npmtrends.com/constantes)
- [Snyk Advisor](https://snyk.io/advisor/npm-package/constantes)

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run lint
```

```bash
npm run format
```

```bash
npm run build
```

## Deployment

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the version in the `API`, `Licenses`, `Package Phobia`, and `bundlejs` links at the top.
- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/constantes/-/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/constantes).
