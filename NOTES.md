# Notes

- https://github.com/SaraVieira/screenscraper-js
- https://github.com/snyk-snippets/modern-npm-package
- https://github.com/arethetypeswrong/arethetypeswrong.github.io/tree/main/packages/cli
- https://github.com/microsoft/TypeScript/issues/49462
- https://github.com/microsoft/FluidFramework/pull/18824/files
- https://github.com/microsoft/FluidFramework/blob/main/packages/drivers/debugger/package.json
- https://github.com/tommy351/tsc-multi
- https://github.com/75lb/renamer
- https://www.npmjs.com/package/renamer
- https://github.com/microsoft/FluidFramework/commit/8ffd6a370eba7e3f3a6c7f4b656f589a01965908
- https://github.com/tylerbutler/FluidFramework/blob/7e1756dafa1bced8517f988bb00a5b616bbdcab2/packages/drivers/driver-web-cache/package.json#L39:
  - `"build:rename-types": "renamer \"lib/**\" -f .d.ts -r .d.mts --force"`
  - `"exports": { ".": { "import": { "types": "./lib/index.d.mts", "default": "./lib/index.mjs" }, "require": { "types": "./dist/index.d.ts", "default": "./dist/index.cjs" } } }`
- https://github.com/tylerbutler/FluidFramework/tree/7e1756dafa1bced8517f988bb00a5b616bbdcab2/common/build/build-common
- https://github.com/sindresorhus/move-file-cli
- https://www.totaltypescript.com/concepts/option-module-must-be-set-to-nodenext-when-option-moduleresolution-is-set-to-nodenext
- https://github.com/privatenumber/pkgroll:
  - https://github.com/privatenumber/pkgroll/issues/51
  - https://github.com/privatenumber/pkgroll/issues/49
- https://github.com/unjs/unbuild:
  - https://github.com/unjs/unbuild/issues/374
  - https://github.com/unjs/unbuild/issues/370
  - https://github.com/unjs/template
  - https://github.com/unjs/unbuild/releases
  - https://github.com/unjs/unbuild/pull/273
  - https://github.com/unjs/unbuild/pull/226
- https://github.com/egoist/tsup:
  - https://github.com/egoist/ts-lib-starter
  - https://github.com/egoist/ts-lib-starter/blob/main/tsconfig.json
  - https://github.com/egoist/tsup/releases
  - https://github.com/egoist/tsup/pull/934/files
  - https://github.com/egoist/tsup/issues/760
  - https://github.com/nostr-dev-kit/ndk/pull/161
  - https://github.com/nostr-dev-kit/ndk/blob/41fdd62860fadad5e6c307a395039afacf52bfe1/ndk/package.json
- https://antfu.me/posts/publish-esm-and-cjs
- https://github.com/u3u/ts-lib-starter
- https://github.com/alexzhang1030/starter-ts
- https://github.com/sxzz/ts-lib-starter
- https://github.com/sachinraja/ts-lib-starter/blob/main/package.json
- https://paka.dev/
- https://github.com/tsconfig/bases/blob/main/bases/strictest.json
- https://www.typescriptlang.org/docs/handbook/modules/theory.html#the-module-output-format
- https://www.typescriptlang.org/docs/handbook/modules/reference.html#the-module-compiler-option
- https://github.com/total-typescript/ts-reset/blob/main/tsconfig.json:
  - `"target": "ES2020", /* Set the JavaScript language version for emitted JavaScript and include compatible library declarations. */`
  - `"module": "commonjs", /* Specify what module code is generated. */`
- https://www.typescriptlang.org/tsconfig#moduleResolution
- https://github.com/unjs/template/blob/main/tsconfig.json
- https://github.com/egoist/tsup/blob/v8.0.2/src/cli-main.ts#L84
- https://github.com/unjs/mdbox/blob/c56c89b0d866e492ece83bf432db1ae8e2634f76/package.json
- https://tsup.egoist.dev/#bundle-formats
- https://codeberg.org/joaopalmeiro/misc-tsconfigs
- https://github.com/joaopalmeiro/template-ts-package
- npm registry:
  - https://github.com/npm/registry
  - https://github.com/npm/registry/blob/master/docs/responses/package-metadata.md
  - https://github.com/npm/registry/blob/master/docs/REGISTRY-API.md
  - https://github.com/npm/registry/blob/master/docs/download-counts.md
- https://github.com/mbostock/isoformat/tree/v0.2.1: `YYYY-MM-DD`

## Commands

```bash
npm install -D typescript misc-tsconfigs @biomejs/biome publint @arethetypeswrong/cli sort-package-json npm-run-all2 npm-package-json-lint npm-package-json-lint-config-package tsup
```

```bash
npx degit github:snyk-snippets/modern-npm-package test-modern-npm-package
```

```bash
cd test/ && npm install && npm run build && npx publint
```

```bash
npx @arethetypeswrong/cli --pack .
```

```bash
npx degit github:egoist/ts-lib-starter test-ts-lib-starter
```

```bash
npx tsc --showConfig
```

```bash
lychee README.md NOTES.md --verbose
```

## Snippets

### https://github.com/u3u/ts-lib-starter/blob/fbda979fdb7f106161496f107c84386f225a1638/package.json

```json
{
  "type": "commonjs",
  "exports": {
    ".": {
      "import": {
        "types": "./dist/index.d.mts",
        "default": "./dist/index.mjs"
      },
      "require": {
        "types": "./dist/index.d.ts",
        "default": "./dist/index.js"
      }
    },
    "./*": {
      "import": {
        "types": "./dist/*.d.mts",
        "default": "./dist/*.mjs"
      },
      "require": {
        "types": "./dist/*.d.ts",
        "default": "./dist/*.js"
      }
    }
  },
  "main": "./dist/index.js",
  "module": "./dist/index.mjs",
  "types": "./dist/index.d.ts"
}
```
