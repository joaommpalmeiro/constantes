export const DONE: string = "Done!";
export const COPIED_CLIPBOARD: string = "Copied to clipboard!";

export const BASE_DOWNLOADS_URL: string = "https://api.npmjs.org/downloads";
export const BASE_REGISTRY_URL: string = "https://registry.npmjs.org";

export const ISO_8601_DATE_FORMAT: string = "YYYY-MM-DD";
export const UTC_TZ: string = "UTC";
